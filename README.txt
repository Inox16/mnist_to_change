説明
　　mnistの手書き文字サンプルを画像化します

構造
　　|
    |- images  画像化した手書き文字を保存するディレクトリ
    |
    |- mnist   mnistのバイナリデータ（手書き文字、ラベル）を入れるディレクトリ
　　|
    |- mnist_to_image.py  バイナリを画像にするプログラム python2系で動作確認


